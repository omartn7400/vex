package drag.vex.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import drag.vex.entity.User;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {
	
	@Autowired
	UserService userService;
	
	@Test
	public void testAddUser (){
		User u = new User("Omar8036","123456789",null);
		User userAdded = userService.addUser(u);
		assertEquals(u.getUsername(), userAdded.getUsername());
	}
	
	@Test
	public void testAuthentification (){
		User userRetreived = userService.authentifier("Omar7400", "123");
		assertEquals(userRetreived.getUsername(), "Omar7400");
	}
	
	@Test
	public void testResetPassword (){
		String email = "omar.naffeti@esprit.tn";
		User usersender = userService.resetPassword(email);
		assertEquals(usersender.getUsername(), "Omar7400");
	}
}
