package drag.vex.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@ToString
public class User {
	
	public enum Gender{
		MALE,FEMALE
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private Long id;
	
	@Column
	@NonNull
	private String username;
	
	@Column
	@NonNull
	private String password;
	
	@Column
	@NonNull
	private String email;
	
	@Column
	private String first_name;
	
	@Column
	private String last_name;
	
	@Column 
	@Temporal(TemporalType.DATE)
	private Date birthday;
	
	@Column
	private String phone_number;
	
	@Column 
	@Enumerated(EnumType.ORDINAL)
	Gender gender;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user_posted")
	private List<Post> posts;
	
	@OneToOne
	private Image profil_picture;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user_notified")
	private List<Notification> notifications;
	
}


	
