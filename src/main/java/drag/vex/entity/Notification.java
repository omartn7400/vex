package drag.vex.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Notification {
	
	public enum NotificationType{
		COMMENT, NEWS
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long id;
	
	@Column
	private String libelle;
	
	@Column
	private String description;
	
	@Column
	@Enumerated(EnumType.ORDINAL)
	private NotificationType type;
	
	@Column
	@Temporal(TemporalType.TIME)
	private Date event_time;
	
	@ManyToOne
	Post post_notification;
	
	@ManyToOne
	Comment comment_notification;
	
	@ManyToOne
	User user_notified;

}
