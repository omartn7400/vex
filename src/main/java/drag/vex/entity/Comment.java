package drag.vex.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Comment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long id;
	
	@Column
	private String libelle;
	
	@Column
	@Temporal(TemporalType.TIME)
	private Date created_at;
	
	@Column
	@Temporal(TemporalType.TIME)
	private Date updated_at;
	
	@Column
	private int upvotes;
	
	@Column
	private int downvotes;
	
	@Column
	private int likes;
	
	@ManyToOne
	Post post_comment;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "comment_image")
	private List<Image> comment_images;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "comment_notification")
	private List<Notification> notifications;
	
}
