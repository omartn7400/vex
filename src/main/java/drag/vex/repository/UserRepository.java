package drag.vex.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import drag.vex.entity.User;

@Repository
@EnableJpaRepositories
public interface UserRepository extends JpaRepository<User, Long>{

	@Query(value = "SELECT * FROM User u WHERE u.username= :username AND u.password= :password", nativeQuery = true)
	List<User> findUserByUsernameAndPassword (@Param("username") String username, @Param("password") String password);
	
	@Query(value = "SELECT * FROM User u WHERE u.email= :email", nativeQuery = true)
	List<User> getByEmail(@Param("email") String email);
}
