package drag.vex.service;

import drag.vex.entity.User;

public interface UserService{

	public User authentifier(String username, String password);
	
	public User addUser (User u);
	
	public User resetPassword(String email);
}
