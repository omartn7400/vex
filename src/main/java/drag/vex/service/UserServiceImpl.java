package drag.vex.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import drag.vex.entity.User;
import drag.vex.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;



@Service
@Slf4j
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Override
	public User authentifier(String username, String password) {
		List<User> listUsers = null;
		try {
			log.info("In authentification method");
			listUsers = userRepository.findUserByUsernameAndPassword(username, password);
			log.info("Out of autthenfication method without issues " + listUsers.get(0).toString());
		} catch (Exception e) {
			log.error("Error in authentification: " + e);
		}
		return listUsers.get(0);
	}

	@Override
	public User addUser(User u) {
		User savedUser = null;
		try {
			log.info("In addUser method");
			savedUser = userRepository.save(u);
			log.info("Out of addUSer method without any issues");
		} catch (Exception e) {
			log.error("Error in addUser: " + e);
		}
		return savedUser; 
	}

	@Override
	public User resetPassword(String email) {
		List<User> u = null;
		try {
			log.info("In resetPassword method");
			u = userRepository.getByEmail(email);
			if (!u.isEmpty()){
				log.info (u.get(0).toString() + u.size());
				SimpleMailMessage emailcontenet = new SimpleMailMessage();
				emailcontenet.setTo(email);
				emailcontenet.setText("Hi " + u.get(0).getFirst_name() + ",\n\n "
									  + "Forgot your password?\n" 
									  + "We received a request to reset the password for your account.\n\n"
									  + "Your password is: " + u.get(0).getPassword() + "\n\n"
									  + "Thank you for using Vex.");
				emailcontenet.setSubject("Password Reset");

		        mailSender.send(emailcontenet);
			}
			log.info("Out of resetPassword without any issues");
		} catch (Exception e) {
			log.error("Error in resetPassword method: " + e);
		}
		
		return u.get(0);
	}
	
	
}
