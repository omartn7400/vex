package drag.vex.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import drag.vex.entity.User;
import drag.vex.service.UserService;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

	@Autowired
	UserService userService;
	
	@PostMapping("/authentification")
	@ResponseBody
	public User getUser(@RequestBody User u){
		User usersList = userService.authentifier(u.getUsername(),u.getPassword());
		return usersList;
	}
	
	@PostMapping("/resetpass")
	@ResponseBody
	public User resetPasswordController (@RequestBody User u){
		log.info(u.getEmail());
		User userSender = userService.resetPassword(u.getEmail());
		return userSender;
	}
}
